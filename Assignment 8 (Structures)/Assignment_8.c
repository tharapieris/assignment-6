#include <stdio.h>
#include <string.h>
struct STUDENT
{
    char fname[15];
    char sub[15];
    int marks;
};
int main ()
{
    struct STUDENT stdnt[5];
    int i;
    printf("Enter details of 5 students\n");
    for (i=0; i<5; i++)
    {
        printf("Enter details of student %d\n",i+1);
        printf("First name: ");
        scanf("%s",&stdnt[i].fname);
        printf("Subject: ");
        scanf("%s",&stdnt[i].sub);
        printf("Marks: ");
        scanf("%d",&stdnt[i].marks);
        printf("\n");
    }
    printf("\n");
    printf("Details of the students\n");
    for (i=0; i<5; i++)
    {
        printf("Student %d\n",i+1);
        printf("Name of the student: %s\n",stdnt[i].fname);
        printf("Subject: %s\n",stdnt[i].sub);
        printf("Marks obtained: %d\n",stdnt[i].marks);
        printf("\n");
    }
    return 0;
}